
from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import Select

class LoginToFacebook:

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.get("https://www.facebook.com/")

    def test_Login(self):
        driver = self.driver
        facebookUsername = "nielsgvermeiren@gmail.com"
        facebookPassword = "NiEWk785"
        emailFieldID = "email"
        passFieldID = "pass"
        loginButtonXpath = "//input[@value='Log In']"
        facebookLogo = "/html/body/div/div[1]/div/div/div/div[1]/div/h1/a"

        emailFieldElement = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id(emailFieldID))
        passFieldElement = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id(passFieldID))
        loginButtonElement = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_xpath(loginButtonXpath))

        emailFieldElement.clear()
        emailFieldElement.send_keys(facebookUsername)
        passFieldElement.clear()
        passFieldElement.send_keys(facebookPassword)
        loginButtonElement.click()
        WebDriverWait(driver, 20).until(lambda driver: driver.find_element_by_xpath(facebookLogo))

    def tearDown(self):
        self.driver.quit()

class RegisterToFacebook:

    def __init__(self):
        self.first_name ='Niels'
        self.last_name = 'Fermicelli'
        self.email = 'nielsvermeiren@internet-bots.com'
        self.password = 'Encapsulator154'
        self.sex = 'male'
        self.driver = webdriver.Firefox()
        self.setup()

    def setup(self):
        self.driver.get("https://www.facebook.com/")

    def test_register(self):
        driver = self.driver
        driver.find_element_by_xpath("//*[@name='firstname']").send_keys(self.first_name)
        driver.find_element_by_xpath("//*[@name='lastname']").send_keys(self.last_name)
        driver.find_element_by_xpath("//*[@name='reg_email__']").send_keys(self.email)
        driver.find_element_by_xpath("//*[@name='reg_passwd__']").send_keys(self.password)
        driver.find_element_by_xpath("//*[@name='reg_email_confirmation__']").send_keys(self.email)
        Select(driver.find_element_by_xpath("//*[@id='month']")).select_by_value("2")
        Select(driver.find_element_by_xpath("//*[@id='day']")).select_by_value("1")
        Select(driver.find_element_by_xpath("//*[@id='year']")).select_by_value("1987")

        if self.sex == 'male':
            driver.find_element_by_xpath("//input[@name='sex'and @value=2]").click()
        else:
            driver.find_element_by_xpath("//input[@name='sex'and @value=1]").click()

        driver.find_element_by_xpath("//button[@name='websubmit']").click()

fbr = RegisterToFacebook()
fbr.test_register()